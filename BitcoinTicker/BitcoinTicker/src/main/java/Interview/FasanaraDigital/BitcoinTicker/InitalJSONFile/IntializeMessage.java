package Interview.FasanaraDigital.BitcoinTicker.InitalJSONFile;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

public class IntializeMessage
{
    @PostMapping("/create")
    @ResponseBody
    public Message IntializeMessage(@RequestBody Message InitalMessage) {
        return  InitalMessage;
    }
}
package Interview.FasanaraDigital.BitcoinTicker.InitalJSONFile;

import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;

public class Message {

    public Map<String,String> Operation = new HashMap<String,String>();
    public Map<String,String> Channel = new HashMap<String,String>();
    public Map<String,String> market = new HashMap<String,String>();

    public Message()
    {
        this.Operation.put("op","Subscribe");
        this.Channel.put("channel","ticker");
        this.market.put("market","BTC/USD");
    }

}


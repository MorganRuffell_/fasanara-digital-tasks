package Interview.FasanaraDigital.BitcoinTicker;
//package Interview.FasanaraDigital.BitcoinTicker.messagingstopwebsocket;

import Interview.FasanaraDigital.BitcoinTicker.messagingstopwebsocket.BroadcastName;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.util.HtmlUtils;

import javax.swing.text.html.HTML;

@Controller
public class GreetingController {

    @MessageMapping("/FTXGreeting")
    @SendTo("Bitcoin/Collection")
    public FTXGreeting bitcoinGreet(BroadcastName message) throws Exception{
        return new FTXGreeting("DEBUG, " + HtmlUtils.htmlEscape(message.getTicker()) + "X");
    }

}

package Interview.FasanaraDigital.BitcoinTicker;

import Interview.FasanaraDigital.BitcoinTicker.InitalJSONFile.Message;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.util.StringUtils;

import java.beans.PropertyEditorSupport;
import java.util.*;

public class MessageEditor extends PropertyEditorSupport {

    private ObjectMapper objectMapper;

    public MessageEditor(ObjectMapper objectMapper){
        this.objectMapper = objectMapper;
    }

    public void setAsText(String deserializedText) throws IllegalArgumentException{
        if (StringUtils.hasText(deserializedText)){
            setValue(null);
        }else{

            Message NewMessage = new Message();

            try
            {
                NewMessage = objectMapper.readValue(deserializedText,Message.class);
            }
            catch (JsonProcessingException e)
            {
               throw new IllegalArgumentException();
            }

        }
    }
}

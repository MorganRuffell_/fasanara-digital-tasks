package Interview.FasanaraDigital.BitcoinTicker.InitalJSONFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

public class GetMessage {

    final ObjectMapper mapper = new ObjectMapper();

    @GetMapping("/get")
    @ResponseBody
    public Message GetMessage(@RequestParam String FetchedMessage) throws JsonMappingException, JsonProcessingException {
        Message FetchedData = mapper.readValue(FetchedMessage,Message.class);
        return FetchedData;
    }



}
